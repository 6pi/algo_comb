#include "Instance.hpp"

using namespace std;

//fonction detection sous tour
//fonction inspirée du site de Gurobi | Page: Traveling sales man
int findsubtour(int n, int** sol, int* tour){
	bool seen[n];
	int bestind, bestlen;
	int i, node, len, start;

	//init du tableau de visit a false
	for (int j=0;j<n;++j){
		seen[j] = false;
	}

	start = 0;
	bestlen = n+1;
	bestind = -1;
	node = 0;
	while (start < n) {
		for (node = 0; node < n; node++){
			if (!seen[node])
				break;
		}
		if (node == n){
			break;
		}

		for (len = 0; len < n; len++) {
			tour[start+len] = node;
			seen[node] = true;
			for (i = 0; i < n; i++) {
				if (sol[node][i] > 0.5 && !seen[i]) {
					node = i;
					break;
				}
			}
			if (i == n) {
				len++;
				if (len < bestlen) {
					bestlen = len;
					bestind = start;
				}
				start += len;
				break;
		}
		}
	}

	for (i = 0; i < bestlen; i++){
		tour[i] = tour[bestind+i];

	}
	return bestlen;
}

class SousTourCallback: public GRBCallback{

	public:
	GRBVar **_x;
	int nbCities;
	SousTourCallback(GRBVar **x, int n): _x(x), nbCities(n){};

	protected:
	void callback()
	{
		try {
			//cout<<"onest dedans"<<endl;
			if (where == GRB_CB_MIPSOL) {
				int **x = new int*[nbCities];
				int *tour = new int[nbCities];
				int  len;

				//copie du tableau pour resoudre les probleme de conversion
				for (size_t i = 0; i < nbCities; ++i){
					x[i]= new int[nbCities];
					for(size_t j=0; j<nbCities; ++j ){
						x[i][j] = (int)getSolution(_x[i][j]);
					}
				}

				len = findsubtour(nbCities, x, tour);
				//Si on a un sous tours on ajoute la contrainte (len stock la longueur du sous tours trouvé)
				if (len < nbCities) {
					GRBLinExpr c = 0;
					for (size_t i = 0; i < len; ++i){
						for (size_t j = 0; j < len; ++j){
							c += _x[tour[i]][tour[j]];
						}
					}
					//bloque le sous tours
					addLazy(c <= len-1);
				}

				//suppression des variables crées
				for (size_t i = 0; i < nbCities; ++i){
					delete[] x[i];
				}
				delete[] x;
				delete[] tour;
			}
		} catch (GRBException e) {
			cout << "Error number: " << e.getErrorCode() << endl;
			cout << e.getMessage() << endl;
		} catch (...) {
			cout << "Error during callback" << endl;
		}
	}
};

int Instance::sous_tours_solver(){
	int nbCities = this->dim;
	vector<vector<int> > distance = this->array;
	GRBVar** x;

	try{
		// --- Creation of the Gurobi environment ---
		cout<<"--> Creating the Gurobi environment"<<endl;
		GRBEnv env = GRBEnv(true);
		//env.set("LogFile", "mip1.log"); ///< prints the log in a file
		env.start();

		// --- Creation of the Gurobi model ---
		cout<<"--> Creating the Gurobi model"<<endl;
		GRBModel model = GRBModel(env);

		// --- Creation of the variables ---
		cout<<"--> Creating the variables"<<endl;
		x = new GRBVar*[nbCities];
		for(size_t i=0;i<nbCities;++i){
			x[i] = new GRBVar[nbCities];
			for (size_t j = 0; j < nbCities; ++j) {
				stringstream ss;
				ss << "x(" << i << ","<< j<<")";
				x[i][j]=model.addVar(0.0, 1.0, 0.0, GRB_BINARY, ss.str());
			}
		}

		// --- Creation of the objective function ---
		cout<<"--> Creating the objective function"<<endl;
		GRBLinExpr obj = 0;
		for(size_t i=0;i<nbCities;++i){
			for(size_t j=0;j<nbCities;++j){
				obj+=distance[i][j]*x[i][j];
			}
		}
		model.setObjective(obj, GRB_MINIMIZE);

		// --- Creation of the constraints ---
		cout<<"--> Creating the constraints"<<endl;
		for(size_t j=0;j<nbCities;++j){
			GRBLinExpr c1 = 0;
			for(size_t i=0;i<nbCities;++i){
				if (i != j)
					c1+=x[j][i];
			}
			stringstream ss;
			ss << "Demand("<<j<<")";
			model.addConstr(c1==1,ss.str());
		}

		for(size_t j=0;j<nbCities;++j){
			GRBLinExpr c2 = 0;
			for(size_t i=0;i<nbCities;++i){
				if (i != j)
					c2+=x[i][j];
			}
			stringstream ss;
			ss << "Demand("<<j<<")";
			model.addConstr(c2==1,ss.str());
		}

		// Optimize model
		// --- Solver configuration ---
		cout<<"--> Configuring the solver"<<endl;
		model.set(GRB_IntParam_LazyConstraints, 1);
		model.set(GRB_DoubleParam_TimeLimit, 600.0); //< sets the time limit (in seconds)
		model.set(GRB_IntParam_Threads,1); //< limits the solver to single thread usage
		SousTourCallback *callback = new SousTourCallback(x, nbCities);
		model.setCallback(callback);

		// --- Solver launch ---
		cout<<"--> Running the solver"<<endl;
		model.optimize();
		model.write("model.lp"); //< Writes the model in a file


		// --- Solver results retrieval ---
		cout<<"--> Retrieving solver results "<<endl;

		int status = model.get(GRB_IntAttr_Status);
		if (status == GRB_OPTIMAL || (status== GRB_TIME_LIMIT && model.get(GRB_IntAttr_SolCount)>0))
		{
			//the solver has computed the optimal solution or a feasible solution (when the time limit is reached before proving optimality)
			cout << "Succes! (Status: " << status << ")" << endl; //< prints the solver status (see the gurobi documentation)
			cout << "Runtime : " << model.get(GRB_DoubleAttr_Runtime) << " seconds"<<endl;

			cout<<"--> Printing results "<<endl;
			//model.write("solution.sol"); //< Writes the solution in a file
			cout << "Objective value = "<< model.get(GRB_DoubleAttr_ObjVal)  << endl; //<gets the value of the objective function for the best computed solution (optimal if no time limit)
			/**for(size_t i=0;i<nbWarehouses;++i){
			if(z[i].get(GRB_DoubleAttr_X)>=0.5){
			cout << "- Warehouse "<<i<<" is open and has the following assigned customers"<< endl;
			for(size_t j=0;j<nbCustomers;++j){
			if(y[j][i].get(GRB_DoubleAttr_X)>=1e-4){
			cout << "\t> Customer "<<j<<" for "<<y[j][i].get(GRB_DoubleAttr_X)*100<<" % of its demand"<<endl;
		`		}
			}
		}
		}**/
		}
		else
		{
			// the model is infeasible (maybe wrong) or the solver has reached the time limit without finding a feasible solution
			cerr << "Fail! (Status: " << status << ")" << endl; //< see status page in the Gurobi documentation
		}
	}
	catch(GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
	}
	catch(...) {
		cout << "Exception during optimization" << endl;
	}
	return 0;
}
