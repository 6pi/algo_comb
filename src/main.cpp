#include "td2.hpp"

using namespace std;

int main(int argc, char **argv) {
	if (argc != 3)
	{
		cout << "Usage : " << endl;
		cout << "./td2 {mtz|flot|soustours} file.dat" << endl;
	}
	if (argc >= 3)
	{
		Instance inst = parser(string(argv[2]));
		if (strcmp(argv[1], "mtz") == 0)
		{
			inst.mtz_solver();
		}
		else if (strcmp(argv[1], "flot") == 0)
		{
			inst.flot_solver();
		}
		else if (strcmp(argv[1], "soustours") == 0)
		{
			inst.sous_tours_solver();
		}
		else
		{
			cout << "Can't find the right exercise" << endl;
			return 1;
		}
	}

	return 0;
}
