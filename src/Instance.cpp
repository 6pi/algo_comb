#include "Instance.hpp"
using namespace std;

Instance::Instance(int dim, std::vector<std::vector<int> > array, std::string name)
{
	this->dim = dim;
	this->array = array;
	this->name = name;

}
void Instance::display() {
	cout << "Name : " << this->name << endl;
	cout << "Dimension : " << this->dim << endl;
	cout << "Data : " << endl;
	for (int i = 0; i < this->dim; i++)
	{
		for (int j = 0; j < this->dim; j++)
		{
			cout << this->array[i][j] << " ";
		}
		cout << endl;
	}
}
