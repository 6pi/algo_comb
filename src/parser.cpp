#include "parser.hpp"

using namespace std;

void openFile(ifstream &file, string filePath)
{
	file.open(filePath);
	if (file.is_open())
	{
		cout << "File opened successfully" << endl;
	}
	else
	{
		cerr << "File open failed\n" << endl;
		exit(-1);
	}
}

bool isnumeric(char c)
{
	if ('0' <= c && '9' >= c)
		return true;
	return false;
}

Instance processFile(ifstream &file)
{
	string line;
	vector<vector<int> > array;
	string name;
	const char delimiter = ' ';
	int dim;
	int j = 0;
	while (getline(file, line))
	{
		//name
		if (line[0] == 'N') {
			std::stringstream ss(line);
			std::string token;
			getline(ss, token, delimiter);
			getline(ss, token, delimiter);
			name = token;
		}
		if (line[0] == 'D') {
			std::stringstream ss(line);
			std::string token;
			getline(ss, token, delimiter);
			getline(ss, token, delimiter);
			dim = stoi(token);
		}

		if (isnumeric(line[0])){
			std::stringstream ss(line);
			std::string token;
			vector<int> tmp;
			array.push_back(tmp);
			for (int i = 0; i < dim; i++)
			{
				getline(ss, token, delimiter);
				array[j].push_back(stoi(token));
			}
			j++;
		}
	}
	Instance inst = Instance(dim, array, name);
	return inst;
}


Instance parser(string filename)
{
	ifstream file;
	openFile(file, filename);
	Instance instance = processFile(file);
	file.close();
	return instance;
}
