#include "Instance.hpp"

using namespace std;

class SousTourFlotCallback: public GRBCallback{

        public:
        GRBVar ***_x;
		int nbCities;
        SousTourFlotCallback(GRBVar ***x, int n): _x(x), nbCities(n){};

        protected:
        void callback()
		{
            try
			{
				//(int)c.getConstant()

                if(where==GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL){
					//cout<< nbCities <<endl;
					for (size_t i=0;i<nbCities;++i){
						for (size_t j=0;j<nbCities;++j){
							for (size_t k=1;k<nbCities-1;++k){
								GRBLinExpr somme_droite = 0;
								for (size_t l=0;l<nbCities;++l){
									if (l != i)
										somme_droite += _x[j][l][k+1];
								}
								if ((int)somme_droite.getConstant() < (int) getSolution(_x[i][j][k])) {
									//cout << "Sous tours non satisfait."<<endl;
                       				addCut(_x[i][j][k] <= somme_droite);
								}
							}
						}
					}
                }
            }
			catch(GRBException e)
			{
                cout<< "Error number:"<<e.getErrorCode()<<endl;
                cout<< e.getMessage()<<endl;
            }
			catch(...)
			{
				cout<<"Error during callback"<<endl;
			}
        }

    };

int Instance::flot_solver()
{
	int nbCities = this->dim;
	vector<vector<int> > distance = this->array;

	GRBVar*** x = nullptr;
	stringstream ss;
	try{
		// --- Creation of the Gurobi environment ---
		cout<<"--> Creating the Gurobi environment"<<endl;
		GRBEnv env = GRBEnv(true);
		env.set("LogFile", "mip1.log"); ///< prints the log in a file
		env.start();

		// --- Creation of the Gurobi model ---
		cout<<"--> Creating the Gurobi model"<<endl;
		GRBModel model = GRBModel(env);

		// --- Creation of the variables ---
		cout<<"--> Creating the variables"<<endl;
		x = new GRBVar**[nbCities];
		for(size_t i=0;i<nbCities;++i){
			x[i] = new GRBVar*[nbCities];
			for (size_t j = 0; j<nbCities; ++j) {
				x[i][j] = new GRBVar[nbCities+1];
				for (size_t k = 0; k<nbCities+1; ++k) {
					ss << "x(" << i << ","<< j<< "," << k << ")";
					x[i][j][k]=model.addVar(0.0, 1.0, 0.0, GRB_BINARY, ss.str());
					ss.str("");
					ss.clear();
				}
			}
		}

		// --- Creation of the objective function ---
		cout<<"--> Creating the objective function"<<endl;
		GRBLinExpr obj = 0;
		for(size_t i=0;i<nbCities;++i){

			for(size_t j=0;j<nbCities;++j){
				for (size_t k=0; k<nbCities;++k) {
					obj+=distance[i][j]*x[i][j][k];
				}
			}
		}
		model.setObjective(obj, GRB_MINIMIZE);

		// --- Creation of the constraints ---
		cout<<"--> Creating the constraints"<<endl;

		//on commence par larc du sommet 0 au temps 0
		GRBLinExpr c2 = 0;
		for (size_t j=1; j<nbCities; ++j) {
			c2 += x[0][j][0];
		}
		ss << "On commence avec les sommet 0 au temps 0";
		model.addConstr(c2==1,ss.str());
		ss.str("");
		ss.clear();

		//on fini par larc du sommet 0
		GRBLinExpr c3 = 0;
		for (size_t j=1; j<nbCities; ++j) {
			c3 += x[j][0][nbCities-1];
		}
		ss << "On fini avec le sommet 0";
		model.addConstr(c3==1,ss.str());
		ss.str("");
		ss.clear();

		//Un seul chemin pris au temps k

			/*
		for (size_t k=0; k<nbCities; ++k) {
			GRBLinExpr c1 = 0;
			for (size_t i=0; i<nbCities; ++i){
				for (size_t j=0; j<nbCities; ++j) {
					c1 += x[i][j][k];
				}
			}
			ss << "Au temps ("<<k<<") On prend un seul chemin";
			model.addConstr(c1==1,ss.str());
			ss.str("");
			ss.clear();
		}
		*/


		//Toutes les villes ont été visité une fois
		for (size_t i=0; i<nbCities; ++i) {
			GRBLinExpr c4 = 0;
			for (size_t j=0; j<nbCities; ++j) {
				for (size_t k=0; k<nbCities; ++k) {
					c4 += x[j][i][k];
				}
			}
			ss << "La ville " << i << "a été visitée une seule fois";
			model.addConstr(c4==1,ss.str());
			ss.str("");
			ss.clear();
		}

		//Contrainte de precedence

		for (size_t j=1; j<nbCities; ++j)
		{
			for (size_t k=0;k<nbCities-1;++k){
				GRBLinExpr c5_1 = 0;
				GRBLinExpr c5_2 = 0;
				for (size_t i=0; i<nbCities; ++i) {
					c5_1 += x[i][j][k];
					c5_2 += x[j][i][k+1];
				}
				ss << "La ville " << j << "a été quitté";
				model.addConstr(c5_1==c5_2,ss.str());
				ss.str("");
				ss.clear();
			}
		}

		//contrainte amélioration
		for (size_t j=1;j<nbCities;++j) {
			for (size_t k=0;k<nbCities-1;++k) {
				GRBLinExpr c6 = 0;
				for (size_t i=0;i<nbCities;++i) {
					c6 += x[i][j][k] + x[i][j][k+1];
				}
				ss << "La ville " << j << "a été quitté";
				model.addConstr(c6<=1,ss.str());
				ss.str("");
				ss.clear();
			}
		}


		// Optimize model
		// --- Solver configuration ---
		cout<<"--> Configuring the solver"<<endl;
		model.set(GRB_DoubleParam_TimeLimit, 60.0); //< sets the time limit (in seconds)
		model.set(GRB_IntParam_Threads,1); //< limits the solver to single thread usage

		SousTourFlotCallback *callBack = new SousTourFlotCallback(x, nbCities);
		model.setCallback(callBack);

		// --- Solver launch ---
		cout<<"--> Running the solver"<<endl;
		model.optimize();
		model.write("model.lp"); //< Writes the model in a file


		// --- Solver results retrieval ---
		cout<<"--> Retrieving solver results "<<endl;

		int status = model.get(GRB_IntAttr_Status);
		if (status == GRB_OPTIMAL || (status== GRB_TIME_LIMIT && model.get(GRB_IntAttr_SolCount)>0))
		{
			//the solver has computed the optimal solution or a feasible solution (when the time limit is reached before proving optimality)
			cout << "Succes! (Status: " << status << ")" << endl; //< prints the solver status (see the gurobi documentation)
			cout << "Runtime : " << model.get(GRB_DoubleAttr_Runtime) << " seconds"<<endl;

			cout<<"--> Printing results "<<endl;
			//model.write("solution.sol"); //< Writes the solution in a file
			cout << "Objective value = "<< model.get(GRB_DoubleAttr_ObjVal)  << endl; //<gets the value of the objective function for the best computed solution (optimal if no time limit)
			/**for(size_t i=0;i<nbWarehouses;++i){
			if(z[i].get(GRB_DoubleAttr_X)>=0.5){
			cout << "- Warehouse "<<i<<" is open and has the following assigned customers"<< endl;
			for(size_t j=0;j<nbCustomers;++j){
			if(y[j][i].get(GRB_DoubleAttr_X)>=1e-4){
			cout << "\t> Customer "<<j<<" for "<<y[j][i].get(GRB_DoubleAttr_X)*100<<" % of its demand"<<endl;
		}
	}
}
}**/
		}
		else
		{
			// the model is infeasible (maybe wrong) or the solver has reached the time limit without finding a feasible solution
			cerr << "Fail! (Status: " << status << ")" << endl; //< see status page in the Gurobi documentation
		}
		delete callBack;
	}
	catch(GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
		return 1;
	}
	catch(...)
	{
		cout << "Exception during optimization" << endl;
		return 1;
	}
	return 0;
}
