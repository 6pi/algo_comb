#ifndef TD2_HPP
 #define TD2_HPP
#include "Instance.hpp"
#include <cstring>

using namespace std;

int mtz_solver(int nbCities, vector<vector<int> > dist);
Instance parser(string filename);

#endif
