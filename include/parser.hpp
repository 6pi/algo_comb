#ifndef PARSER_HPP
#define PARSER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include "Instance.hpp"


Instance parser(std::string filename);


#endif
