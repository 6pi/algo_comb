#ifndef INSTANCE_HPP
#define INSTANCE_HPP
#include <vector>
#include <string>
#include <iostream>
#include <string>
#include <sstream>
#include <climits>
#include "gurobi_c++.h"

class Instance
{
public:
	int dim;
	std::vector<std::vector<int> > array;
	std::string name;

	Instance(int dim, std::vector<std::vector<int> > array, std::string name);
	void display();
	int mtz_solver();
	int flot_solver();
	int sous_tours_solver();
};

#endif
